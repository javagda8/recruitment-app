Your goal is to implement a flight aggregation service for an online travel agency. This component will be used for listing all available flights when preparing a broader holiday offer for the customer (eg. flights "from Amsterdam to Barcelona on 01 April 2015" could be presented as an addition to a hotel booking made in Barcelona on that date).
In this scenario we have two suppliers (lowcost & regular flight supplier), that provide flight definitions in slightly different format. Both are accessed using a Web Service, which is represented as a Java stub in an isolated package (LowcostFlightSupplierWS & RegularFlightSupplierWS). Your task is to implement a new service that will aggregate results from both services and expose them as a sequence of objects in a common format.

Rules:
Feel free to use any libraries from the Maven Central Repository. Basic Maven and Gradle configs are at your disposal..
Your commit log should reflect real life development process (i.e. make often commits, write some comments etc.).
You can alter or remove anything, apart from LowcostFlightSupplierWS and RegularFlightSupplierWS.

Guidelines:
This is an "open" assignment, which can be implemented in multiple ways and with different complexity in mind. What we're particularly interested in is how you approach a real life business problem from a development perspective. Please consider the following when working on this assignment:
Business usability (e.g. the contract of the service should be clear and extensible)
Design patterns
Performance considerations
Error handling, maintainability & general code quality

